import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { decrement, increment, sign_in } from "./actions";


function App() {
	const counter = useSelector(state => state.counter);
	const isLogged = useSelector(state => state.isLogged);
	const dispatch = useDispatch();

	return (
		<div className="App">
			<h1>Counter: {counter}</h1>

			<button onClick={() => dispatch(increment(2))}>+</button>
			<button onClick={() => dispatch(decrement(3))}>-</button>

			<br/>
			<br/>

			<button onClick={() => dispatch(sign_in())}>{isLogged
				? "Log out"
				: "Log in"}</button>

			{isLogged &&
			<h3>Valuable information I shouldn't see if I'm not logged</h3>}
		</div>
	);
}

export default App;
