export const increment = (number) => ({
	type: "INCREMENT",
	payload: number,
});

export const decrement = (number) => ({
	type: "DECREMENT",
	payload: number,
});

export const sign_in = () => ({
	type: "SIGN_IN",
});