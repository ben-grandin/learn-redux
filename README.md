# Learn redux
A tiny project made following a youtube tutorial : [Redux For Beginners | React Redux Tutorial
](https://www.youtube.com/watch?v=CVpUuw9XSjY) by [Dev Ed](https://www.youtube.com/channel/UClb90NQQcskPUGDIXsQEz5Q)


## More
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## License
The repo is available as open source under the terms of the MIT License.